from context import package as p,submodule
from copy import deepcopy
import pytest
data = submodule('data')

DICT = { 'a': 'b', 'x': { 'y': 'z' } }
DICT_FLAT = { 'a': 'b', 'x': 'y' }
DICT_DEFAULT = { 'f' : 'g' }
LIST = [ 'a', 'b', ['x', 'y', 'z'] ]
LIST_FLAT = [ 'a', 'b', 'x', 'y', 'z' ]
LIST_DEFAULT = list(range(10))
DICT_CROSS = deepcopy(DICT)
DICT_CROSS['_'] = deepcopy(LIST)
LIST_CROSS = deepcopy(LIST)
LIST_CROSS.append(deepcopy(DICT))

def test_dictproxy_read():
    x = deepcopy(DICT)
    p = data.Proxy(x)
    with pytest.raises(ValueError):
        _ = p['@']
    with pytest.raises(KeyError):
        _ = p['c']
    assert isinstance(p, dict)
    assert p['a'] == x['a']
    assert p['x.y'] == x['x']['y']
    assert p['x','y'] == x['x']['y']
    p = p['x']
    assert isinstance(p, dict)
    assert isinstance(p, data.ProxyType)
    assert p['y'] == x['x']['y']

def test_listproxy_read():
    x = deepcopy(LIST)
    p = data.Proxy(x)
    with pytest.raises(IndexError):
        _ = p[3]
    assert isinstance(p, list)
    assert p[1] == x[1]
    assert p[2,0] == x[2][0]
    assert p['2.0'] == x[2][0]
    assert p['2',0] == x[2][0]
    p = p[2]
    assert isinstance(p, list)
    assert isinstance(p, data.ProxyType)
    assert p[1] == x[2][1]
    assert list.__len__(p) == 2

def test_cross_read():
    p = data.Proxy(deepcopy(DICT_CROSS))
    assert p['x']['y'] == 'z'
    assert p['_'][0] == 'a'
    p = data.Proxy(deepcopy(LIST_CROSS))
    assert p[2][0] == 'x'
    assert p[3]['x']['y'] == 'z'

def test_proxy_empty():
    p = data.ListProxy()
    assert len(p) == 0
    p = data.DictProxy()
    assert len(p) == 0

def test_proxy_errors():
    class X(object): pass
    with pytest.raises(data.CustomTypeError):
        _ = data.Proxy(X())
    with pytest.raises(TypeError):
        _ = data.DictProxy(42)
    with pytest.raises(TypeError):
        _ = data.ListProxy(42)
    with pytest.raises(TypeError):
        _ = data.DictProxy(defaults=42)
    with pytest.raises(TypeError):
        _ = data.ListProxy(defaults=42)

def test_dictproxy_default():
    p = data.Proxy(deepcopy(DICT), defaults=deepcopy(DICT_DEFAULT))
    assert p['a'] == 'b'
    assert not dict.__contains__(p,'f')
    assert p['f'] == 'g'
    assert not dict.__contains__(p,'f')

def test_listproxy_default():
    p = data.Proxy(deepcopy(LIST), defaults=deepcopy(LIST_DEFAULT))
    assert p[0] == 'a'
    assert not list.__contains__(p,9)
    assert p[9] == 9
    assert not list.__contains__(p,9)

def test_dictproxy_default_inheritance():
    p = data.Proxy(deepcopy(DICT), defaults=deepcopy(DICT_DEFAULT))
    assert p['a'] == 'b'
    assert p['f'] == 'g'
    p = p['x']
    assert p['y'] == 'z'
    assert p['f'] == 'g'

def test_listproxy_default_inheritance():
    p = data.Proxy(deepcopy(LIST), defaults=deepcopy(LIST_DEFAULT))
    assert p[0] == 'a'
    assert p[3] == 3
    p = p[2]
    assert p[0] == 'x'
    assert p[3] == 3

def test_cross_default_inheritance():
    p = data.Proxy(deepcopy(DICT_CROSS), defaults=deepcopy(LIST_DEFAULT))
    with pytest.raises(KeyError):
        _ = p[0]
    p = p['_']
    assert p[0] == 'a'
    assert p[5] == 5
    p = data.Proxy(deepcopy(LIST_CROSS), defaults=deepcopy(DICT_DEFAULT))
    with pytest.raises(TypeError):
        _ = p['f']
    p = p[3]
    assert p['a'] == 'b'
    assert p['f'] == 'g'

def test_dictproxy_keys():
    p = data.Proxy(deepcopy(DICT))
    assert set(DICT.keys()) == set(p.keys())

def test_dictproxy_values():
    p = data.Proxy(deepcopy(DICT_FLAT))
    assert set(DICT_FLAT.values()) == set(p.values())

def test_dictproxy_items():
    p = data.Proxy(deepcopy(DICT_FLAT))
    assert set(DICT_FLAT.items()) == set(p.items())

def test_dictproxy_iter():
    p = data.Proxy(deepcopy(DICT_FLAT))
    assert set(iter(DICT_FLAT)) == set(iter(p))

def test_dictproxy_contains():
    p = data.Proxy(deepcopy(DICT))
    for k in "abxy":
        assert (k in DICT) == (k in p)

def test_dictproxy_len():
    p = data.Proxy(deepcopy(DICT))
    assert len(DICT) == len(p)

def test_listproxy_iter():
    p = data.Proxy(deepcopy(LIST_FLAT))
    assert list(iter(LIST_FLAT)) == list(iter(p))

def test_listproxy_contains():
    p = data.Proxy(deepcopy(LIST_FLAT))
    for k in [0,'a','b','x','y',1,2,3]:
        assert (k in LIST_FLAT) == (k in p)

def test_listproxy_len():
    p = data.Proxy(deepcopy(LIST))
    assert len(LIST) == len(p)

def test_dictproxy_write():
    d = deepcopy(DICT_FLAT)
    p = data.Proxy(d)
    p['x'] = 'z'
    assert p['x'] == 'z'
    assert d['x'] == 'z'
    assert 'y' not in p
    assert 'y' not in d
    p['y'] = 'z'
    assert p['y'] == 'z'
    assert d['y'] == 'z'
    assert p.setdefault('foo','bar') == 'bar'
    assert p['foo'] == 'bar'
    assert d['foo'] == 'bar'
    p['z.a.b.c.d'] = 'e'
    assert p['z','a','b','c','d'] == 'e'
    assert len(p['z','a','b','c']) == 1

def test_listproxy_write():
    l = deepcopy(LIST_FLAT)
    p = data.Proxy(l)
    p[0] = 42
    assert p[0] == 42
    assert l[0] == 42
    x = len(l)
    assert x not in p
    assert x not in l
    with pytest.raises(IndexError):
        p[x] = 42
    p.append(42)
    assert p[x] == 42
    assert l[x] == 42
    x = len(l)
    p.extend(LIST_FLAT)
    for i,e in enumerate(LIST_FLAT):
        assert p[x+i] == e
        assert l[x+i] == e

def test_dictproxy_delitem():
    d = deepcopy(DICT_FLAT)
    p = data.Proxy(d)
    with pytest.raises(KeyError):
        del p['e']
    del p['x']
    assert 'x' not in d
    assert 'x' not in p

def test_listproxy_delitem():
    l = deepcopy(LIST_FLAT)
    p = data.Proxy(l)
    with pytest.raises(IndexError):
        del p[5]
    x = p[3]
    del p[2]
    assert p[2] == x
    assert l[2] == x
