from context import package as p,submodule,testdir
from copy import deepcopy
import pytest
file = submodule('file')
data = submodule('data')

cfg_path = testdir/'config'

import json
with (cfg_path/'types.json').open('r') as stream:
    CFG_TYPES = json.load(stream)

types = {
        'int': int,
        'float': float,
        'bool': bool,
        'list': list,
        'dict': dict,
        'str': str
        }


@pytest.mark.parametrize('filename',['types.yml','types.json'])
def test_config_types_load(filename):
    cfg = file.ConfigFile(cfg_path/filename)
    assert cfg == CFG_TYPES
    for key,value in cfg.items():
        assert isinstance(value,types[key])
        if key in ['list','dict']:
            assert isinstance(value,data.ProxyType)

@pytest.mark.parametrize('filename',['types.yml','types.json'])
def test_config_reload(tmp_path,filename):
    local_cfg = tmp_path / filename
    assert local_cfg.write_text("{}\n") == 3
    test_cfg = cfg_path / filename
    cfg = file.ConfigFile(local_cfg)
    assert cfg == {}
    with local_cfg.open('w') as w, test_cfg.open('r') as r:
        w.write(r.read())
    cfg.load()
    assert cfg == CFG_TYPES

@pytest.mark.parametrize('filename',['types.yml','types.json'])
def test_config_dump(tmp_path,filename):
    local_cfg = tmp_path / filename
    test_cfg = data.Proxy(deepcopy(CFG_TYPES))
    test_cfg.dump(local_cfg)
    cfg = file.ConfigFile(local_cfg)
    assert cfg == CFG_TYPES
    assert cfg == test_cfg
    assert test_cfg['list'][-1] == CFG_TYPES['list'][-1]
    test_cfg['list'].pop()
    test_cfg.popitem()
    dict(test_cfg.items())
    test_cfg.popitem()
    test_cfg.dump()
    cfg.load()
    assert cfg == test_cfg

@pytest.mark.parametrize('filename',['list.yml','list.json'])
def test_list_config(filename):
    with pytest.raises(TypeError):
        cfg = file.ConfigFile(cfg_path/filename)
