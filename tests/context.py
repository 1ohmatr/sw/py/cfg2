import os,sys
sys.path.insert(0,os.path.abspath(os.path.join(os.path.dirname(__file__),'..')))
import setup
from importlib import import_module
package = import_module(setup.MODULE_NAME)
def submodule(*path):
    name = '.'+'.'.join(path)
    return import_module(name,package=setup.MODULE_NAME)
import pathlib2
testdir = pathlib2.Path(__file__).absolute().parent
__all__=['package','submodule','testdir']
