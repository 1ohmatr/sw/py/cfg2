|pipeline| |coverage| |rtd|

.. |pipeline| image:: https://framagit.org/1ohmatr/sw/py/cfg2/badges/master/pipeline.svg

.. |coverage| image:: https://framagit.org/1ohmatr/sw/py/cfg2/badges/master/coverage.svg

.. |rtd| image:: https://readthedocs.org/projects/cfg2/badge/?version=latest

Python package generated from template https://framagit.org/1ohmatr/sw/py/template
