Logging when writing code
=========================

The :py:mod:`cfg._logging` submodule provides a :py:data:`cfg._logging.logger` object that you can use to get your own logger.

.. code-block:: python

    from ._logger import logger
    log = logger.getChild('submodule_name')
