cfg2 |release|
==================

.. include:: ../README.rst

.. toctree::
   :maxdepth: 4
   :caption: Contents
   :glob:

   package/*

   dev/*
